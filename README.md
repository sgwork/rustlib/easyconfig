# Welcome to easyconfig for Rust

[![](https://badgen.net/github/license/sguerri/rust-easyconfig)](https://www.gnu.org/licenses/)
[![](https://badgen.net/badge/Open%20Source%20%3F/Yes%21/blue?icon=github)](#)

> Easy config for cli application

TODO description

**Main features**
* TODO

**Roadmap**
* TODO

---

- [Welcome to easyconfig for Rust](#welcome-to-easyconfig-for-rust)
  - [Installation](#installation)
  - [Usage](#usage)
  - [Build](#build)
  - [Dependencies](#dependencies)
  - [Author](#author)
  - [Issues](#issues)
  - [License](#license)

## Installation

## Usage

## Build

## Dependencies

- [directories](https://crates.io/crates/directories)
- [inquire](https://crates.io/crates/inquire)
- [serde](https://crates.io/crates/serde)
- [serde_yaml](https://crates.io/crates/serde_yaml)

## Author

Sébastien Guerri - [github page](https://github.com/sguerri)

## Issues

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://github.com/sguerri/rust-easyconfig/issues). You can also contact me.

## License

Copyright (C) 2023 Sebastien Guerri

easyconfig is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

easyconfig is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with easyconfig. If not, see <https://www.gnu.org/licenses/>.
